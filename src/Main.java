import DataAccessLayer.FileWriter;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setScene(new Scene(root, 600, 600));
        primaryStage.show();
        primaryStage.setOnCloseRequest(e -> { FileWriter.getInstance().serialize(); Platform.exit(); });
    }
    public static void main(String[] args) {

        FileWriter.getInstance().fileName=args[0];
        launch(args)
        ;
    }
}
