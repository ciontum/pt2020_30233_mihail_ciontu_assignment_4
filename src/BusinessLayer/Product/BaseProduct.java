package BusinessLayer.Product;
/**
 * Simple product
 */
public class BaseProduct extends MenuItem {
    public  BaseProduct(String name,int price){
            super(name,price);
    }
    @Override
    public int computePrice() {
        return this.price.get();
    }
}
