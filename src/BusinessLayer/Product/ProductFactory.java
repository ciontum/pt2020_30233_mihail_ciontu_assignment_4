package BusinessLayer.Product;
/**
 * Interface for factory method.
 * @see ConcreteProductFactory
 */
public interface ProductFactory {
    MenuItem createProduct(String type, String name, int price);
}
