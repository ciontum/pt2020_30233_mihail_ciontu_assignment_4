package BusinessLayer.Product;


/**
 * Implements Factory method for creating products
 */
public class ConcreteProductFactory implements ProductFactory {
    @Override
    public MenuItem createProduct(String type, String name, int price) {
        if(type=="base")
            return new BaseProduct(name,price);
        if(type=="composed")
            return new CompositeProduct(name);
        return null;
    }
}
