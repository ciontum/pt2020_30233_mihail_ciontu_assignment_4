package BusinessLayer.Product;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

import java.util.ArrayList;

public abstract class MenuItem {
    public SimpleStringProperty name;
    public SimpleIntegerProperty price;

    public MenuItem(String name,int price){
        this.price=new SimpleIntegerProperty(price);
        this.name=new SimpleStringProperty(name);
    }

    public MenuItem(String name){
        this.name=new SimpleStringProperty(name);
    }
    public abstract int computePrice();
    public String getName(){
        return name.get();
    }
    public int getPrice(){
        return price.get();
    }
    public void setName(String name){
        this.name.set(name);
    }
    public void setPrice(int price){
        this.price.set(price);
    }
}
