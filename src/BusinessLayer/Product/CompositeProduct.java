package BusinessLayer.Product;

import javafx.beans.property.SimpleIntegerProperty;

import java.util.ArrayList;
/**
 * Composited product, that it's constructed from more BaseProducts
 */
public class CompositeProduct extends MenuItem {
    ArrayList<MenuItem> products;
    public CompositeProduct(String name){
        super(name);
        this.price=new SimpleIntegerProperty(0);
        products=new ArrayList<>();
    }

    public void add(MenuItem menuItem){

        this.products.add(menuItem);
        System.out.println(this.price.getValue());
        int newPrice=this.price.getValue()+menuItem.computePrice();
        this.price=new SimpleIntegerProperty(newPrice);
    }
    public ArrayList<MenuItem> getComposedItems(){
        return products;
    }
    @Override
    public int computePrice() {
        int computedPrice=0;
        for(MenuItem menuItem: products){
            computedPrice += menuItem.getPrice();
        }
        return computedPrice;
    }
}
