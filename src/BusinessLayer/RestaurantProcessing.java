package BusinessLayer;

import BusinessLayer.Product.MenuItem;
import com.google.java.contract.Requires;

import java.util.ArrayList;
import java.util.HashMap;
/**
 * Interface that defines admin and waiter's available actions on products and orders
 * @see Restaurant
 */
public interface RestaurantProcessing {
    @Requires("name!=null,price>0")
    void addMenuItem(String name, int price,boolean withSave);
    @Requires("name!=null")
    void deleteMenuItem(String name);
    @Requires("name!=null")
    void editMenuItem(String name,String newName,int newPrice);
    boolean createOrder(ArrayList<MenuItem> menuItems, Order order);
    HashMap<Order,ArrayList<MenuItem>> getOrders();
    @Requires("tableNumber>0")
    void generateBill(int tableNumber);

}
