package BusinessLayer;

import javafx.beans.property.SimpleIntegerProperty;

import java.util.Date;
/**
 * Order class's main interest is in orderId field.It hold the instantiated table number.
 */
public class Order {
    private int orderId;
    private Date Date;

    public Order(Date date, int table) {
        date = date;
        this.orderId = table;
    }

    public int getOrderId() {
        return orderId;
    }

    public Order getOrderById(int orderId){
        if(orderId==this.orderId)
            return this;
        return null;
    }

    @Override
    public int hashCode() {
        return orderId;
    }

    @Override
    public boolean equals(Object order) {
        return orderId==((Order)order).orderId;
    }
}
