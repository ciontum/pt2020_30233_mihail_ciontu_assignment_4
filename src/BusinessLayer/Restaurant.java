package BusinessLayer;
import BusinessLayer.Product.ConcreteProductFactory;
import BusinessLayer.Product.MenuItem;
import DataAccessLayer.FileWriter;
import DataAccessLayer.SerializableClass;
import PresentationalLayer.GUI.ChefGUI;
import com.google.java.contract.Invariant;
import com.google.java.contract.Requires;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;

/**
 * Main class,has orders and menu products.Restaurant class implemets admin and waiter's actions.
 */

@Invariant({"fileWriter != null", "menuItems instanceOf HashMap"})
public class Restaurant extends Observable implements RestaurantProcessing {
    private int orderId;
    private HashMap<String,Integer> menuItems;
    private FileWriter fileWriter;
    private ConcreteProductFactory productFactory;
    private HashMap<Order,ArrayList<MenuItem>> orders;
    private ChefGUI chefGUI;
    public Restaurant(){
        menuItems=new HashMap();
        fileWriter=FileWriter.getInstance();
        productFactory=new ConcreteProductFactory();
        orders=new HashMap<>();
        chefGUI=new ChefGUI(this);
    }
    @Override
    @Requires("productName!=null,price>0")
    public void addMenuItem(String productName, int price,boolean withSave){
        MenuItem baseProduct=productFactory.createProduct("base",productName,price);
        this.menuItems.put(baseProduct.getName(),baseProduct.getPrice());
        if(withSave) {
            fileWriter.constructSerialItems(new SerializableClass(baseProduct.getName(), baseProduct.getPrice()));
        }
    }
    @Override
    public void deleteMenuItem(String name) {
                this.menuItems.remove(name);
                fileWriter.deconstructSerialItems(name);
    }

    @Override
    public void editMenuItem(String name,String newName,int newPrice) {
        menuItems.remove(name);
        menuItems.put(newName,newPrice);
        fileWriter.deconstructSerialItems(name);
        fileWriter.constructSerialItems(new SerializableClass(newName,newPrice));
    }

    @Override
    public boolean createOrder(ArrayList<MenuItem> menuItems,Order order) {
        try {
            if (orders.containsKey(order))
                throw new Exception("Table exists!");
        }catch(Exception e){
            return false;
        }
        orders.put(order,menuItems);
         setOrderId(order.getOrderId());
         return true;
    }
    public void gotOrder(){
        setChanged();
        notifyObservers();
    }
    public void setOrderId(int orderId){
        this.orderId=orderId;
        gotOrder();
    }
    public int getOrderId(){
        return orderId;
    }


    @Override
    public HashMap<Order, ArrayList<MenuItem>> getOrders() {
        return orders;
    }
    private Order searchForOrder(int tableNumber){
        Order requestedOrder=null;
        for(Order order : orders.keySet()) {
            requestedOrder=order.getOrderById(tableNumber);
            if(requestedOrder!=null) {
                break;
            }
        }
        return requestedOrder;
    }
    private void writeBill(Order requestedOrder,int tableNumber){
        FileOutputStream out = null;
        try {
            fileWriter.generateBill(tableNumber,requestedOrder,orders.get(requestedOrder));
        } catch (IOException e) {
            System.out.println(e);
        }
    }
    private void removeOrderFromMap(Order requestedOrder){
        orders.remove(requestedOrder);
    }
    @Override
    public void generateBill(int tableNumber) {
        Order requestedOrder=searchForOrder(tableNumber);

        if(requestedOrder!=null) {
            writeBill(requestedOrder,tableNumber);
            removeOrderFromMap(requestedOrder);
        }
    }
}
