package DataAccessLayer;

import BusinessLayer.Product.MenuItem;
import BusinessLayer.Order;

import java.io.*;
import java.util.ArrayList;
/**
 * Class that manipulates files.Used for serialize,deserialize files,also for generating bills.
 */
public class FileWriter {
    public String fileName;
    private ArrayList<SerializableClass> serialItems;
    private static FileWriter fileWriterInstance;
    private FileWriter(){
        serialItems=new ArrayList<>();
    }

    public static FileWriter getInstance(){
        if(fileWriterInstance==null)
            fileWriterInstance=new FileWriter();
        return fileWriterInstance;
    }

    public void constructSerialItems(SerializableClass serial){
        serialItems.add(serial);
    }

    public void deconstructSerialItems(String name){
        for(SerializableClass serialItem :serialItems){
            if(name==serialItem.getName()){
                serialItems.remove(serialItem);
                break;
            }
        }
    }

    public void serialize(){
        try{
            FileOutputStream file=new FileOutputStream(fileName);
            ObjectOutputStream out=new ObjectOutputStream(file);
            out.writeObject(serialItems);
            out.close();
            file.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public ArrayList<SerializableClass> deserialize(){
        FileInputStream file= null;
        try {
            file = new FileInputStream(fileName);
            ObjectInputStream in = new ObjectInputStream(file);
             serialItems = (ArrayList) in.readObject();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            //e.printStackTrace();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return serialItems;
    }
    public void generateBill(int tableNumber, Order requestedOrder,ArrayList<MenuItem> orders) throws IOException{
        FileOutputStream out = null;
        int computedPrice=0;
        out = new FileOutputStream("table"+tableNumber+".txt");
        out.write(("Table "+tableNumber).getBytes());
        out.write(System.getProperty("line.separator").getBytes());
        for(MenuItem orderedItem : orders) {
            computedPrice +=orderedItem.computePrice();
            out.write((orderedItem.getName() + " " + orderedItem.getPrice()).getBytes());
            out.write(System.getProperty("line.separator").getBytes());
        }
        out.write(("Total="+computedPrice).getBytes());
        out.close();
    }
}
