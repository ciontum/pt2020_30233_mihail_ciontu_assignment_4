package DataAccessLayer;

import java.io.Serializable;
import java.util.ArrayList;
/**
 * Class that creates object for serializing.
 */
public class SerializableClass implements Serializable {
     private String name;
     private int price;
    public SerializableClass(String name,int price){
        this.name=name;
        this.price=price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
