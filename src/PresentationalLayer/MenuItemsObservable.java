package PresentationalLayer;

import BusinessLayer.Product.MenuItem;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.ArrayList;
/**
 * Menu UI singleton.Holds all the instantiated menu items.
 */
public class MenuItemsObservable {

    private ObservableList<MenuItem> menuItemsList;
    private static MenuItemsObservable menuItemsObservableInstance;
    private MenuItemsObservable(){
        menuItemsList=FXCollections.observableArrayList();
    }
    public static MenuItemsObservable getInstance(){
        if(menuItemsObservableInstance==null)
            menuItemsObservableInstance=new MenuItemsObservable();
        return menuItemsObservableInstance;
    }
    public void setMenuItemsList(ArrayList arrayList){
        menuItemsList= FXCollections.observableArrayList(arrayList);
    }

    public ObservableList<MenuItem> getMenuItemsList(){
        return menuItemsList;
    }

    public void addMenuItemToList(MenuItem menuItem){
        menuItemsList.add(menuItem);
    }
    public void removeMenuItemFromList(MenuItem menuItem){
        menuItemsList.remove(menuItem);
    }
    public void assignDeserialized(ArrayList<MenuItem> menuItems){
        this.setMenuItemsList(menuItems);
    }

}


