package PresentationalLayer;


import PresentationalLayer.GUI.AdminGUI;
import PresentationalLayer.GUI.GeneralUI;
import PresentationalLayer.GUI.WaiterGUI;
import javafx.scene.control.TableView;
/**
 * Utility class,that resolves an algorithm based on the instance passed by composition.
 */
public class Initializer {
    GeneralUI ui;
    public Initializer(GeneralUI ui){
        this.ui=ui;
    }
    public TableView initializeTableWithUtilities(){
        TableView table=null;

        if(ui instanceof AdminGUI) {
            table=ui.initializeTable(true);

            ui.initializeUtilities(table);
            ((AdminGUI)ui).initializeActions();

            ((AdminGUI)ui).initializeUtilities();
            return table;
        }
        else
            if(ui instanceof WaiterGUI)
        {
            table=ui.initializeTable(false);
            ui.initializeUtilities(table);
            ((WaiterGUI)ui).initializeActions();
            ((WaiterGUI)ui).initializeUtilityBox();
            ((WaiterGUI)ui).initializeOrdersVbox();
            return table;

        }
        System.out.println(table);
            return table;
    }
}
