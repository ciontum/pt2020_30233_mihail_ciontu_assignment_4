package PresentationalLayer.GUI;

import BusinessLayer.*;
import BusinessLayer.Product.MenuItem;
import BusinessLayer.Product.ConcreteProductFactory;
import DataAccessLayer.FileWriter;
import DataAccessLayer.SerializableClass;
import PresentationalLayer.MenuItemsObservable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.util.converter.IntegerStringConverter;

import java.util.ArrayList;
import java.util.HashMap;
/**
 * Abstract class that is inherited by Admin and Waiter.Includes common code.
 * @see AdminGUI
 * @see WaiterGUI
 */
public abstract class GeneralUI {

    protected RestaurantProcessing commands;
    protected ObservableList<MenuItem> menuItemsList;
    protected ConcreteProductFactory productFactory;
    protected HashMap<Order,ArrayList<MenuItem>> ordersList;
    protected MenuItemsObservable menuItemsObservableInstance;

    public GeneralUI() {
        commands = new Restaurant();
        productFactory = new ConcreteProductFactory();
        menuItemsObservableInstance = MenuItemsObservable.getInstance();
        ordersList=commands.getOrders();
    }
    public void initializeUtilities(TableView table){

        initializeVerticalUtility();
        initializeMultiSelectMode(table);


    }
    public abstract void initializeVerticalUtility();
    public abstract void initializeMultiSelectMode(TableView table);

    public void deserializeMenuItems() {
        ArrayList<SerializableClass> deserializedItems;
        deserializedItems = FileWriter.getInstance().deserialize();
        assignToMenuItemList(deserializedItems);
    }
    public void assignToMenuItemList(ArrayList<SerializableClass> deserializedItems){
        ArrayList<MenuItem> menuItems = new ArrayList<>();
        for (SerializableClass serialItem : deserializedItems) {
            commands.addMenuItem(serialItem.getName(), serialItem.getPrice(), false);
            menuItems.add(productFactory.createProduct("base", serialItem.getName(), serialItem.getPrice()));
        }
        menuItemsObservableInstance.assignDeserialized(menuItems);

    }
    public ObservableList<MenuItem> getMenuItemsList() {
        return menuItemsList;
    }

    public void setOnEdit(TableColumn tableColumn, boolean isPrice) {
        tableColumn.setOnEditCommit(
                new EventHandler<TableColumn.CellEditEvent>() {
                    @Override
                    public void handle(TableColumn.CellEditEvent cellEditEvent) {
                        MenuItem menuItem = ((MenuItem) cellEditEvent.getTableView().getItems().get(
                                cellEditEvent.getTablePosition().getRow()));
                        if (!isPrice) {
                            commands.editMenuItem(menuItem.getName(), String.valueOf(cellEditEvent.getNewValue()), menuItem.getPrice());
                            menuItem.setName(String.valueOf(cellEditEvent.getNewValue()));
                        } else {
                            commands.editMenuItem(menuItem.getName(), menuItem.getName(), Integer.parseInt(String.valueOf(cellEditEvent.getNewValue())));
                            (menuItem).setPrice(Integer.parseInt(String.valueOf(cellEditEvent.getNewValue())));
                        }
                    }
                }
        );
    }
    public ObservableList<TablePosition> onMultiSelect(TableView table){
        ObservableList<TablePosition> selectedCells = FXCollections.observableArrayList();
        table.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                selectedCells.add(
                        (TablePosition) table.getSelectionModel().getSelectedCells().get(
                                table.getSelectionModel().getSelectedCells().size()-1
                        )
                );

                for (TablePosition tp : selectedCells){
                    table.getSelectionModel().select(tp.getRow(), tp.getTableColumn());
                }

            }
    });
    return selectedCells;
    }
    public HBox addUtilBox(Object[] components) throws Exception {
        HBox hBox=new HBox();
        for(Object component:components){
            if(component instanceof TextField)
                hBox.getChildren().add((TextField)component);
            else
            if(component instanceof Button)
                hBox.getChildren().add((Button)component);
            else
                throw new Exception("Invalid component");
        }

        hBox.setSpacing(5);
        return hBox;
    }

    public TableView initializeTable(boolean isAdmin) {
        TableView table = new TableView();

        table.setEditable(true);

        TableColumn productNameColumn=configureTableColumn("Product",isAdmin,300);
        TableColumn priceColumn=configureTableColumn("Price",isAdmin,50);

        table.setItems(menuItemsObservableInstance.getMenuItemsList());
        table.getColumns().addAll(productNameColumn, priceColumn);

        return table;
    }
    private TableColumn configureTableColumn(String columnName,boolean isAdmin,int width){
        TableColumn tableColumn=new TableColumn(columnName);
        if(columnName=="Product")
        {
            tableColumn.setCellValueFactory(new PropertyValueFactory<MenuItem, String>("name"));
            tableColumn.setCellFactory(TextFieldTableCell.forTableColumn());
            if (isAdmin) {
                setOnEdit(tableColumn, false);
            }
                    }
        if(columnName=="Price"){
            tableColumn.setCellValueFactory(new PropertyValueFactory<MenuItem, Integer>("price"));
            tableColumn.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
            if (isAdmin) {
                setOnEdit(tableColumn, true);
            }
        }
        tableColumn.setPrefWidth(width);
        return tableColumn;
    }
    public Node nodeFactory(String promptText, String type){
        Node newNode=null;
        if(type=="TextField") {
            newNode = new TextField();
            ((TextField)newNode).setPromptText(promptText);
        }
        if(type=="Button")
            newNode= new Button(promptText);
        return newNode;
    }

}
