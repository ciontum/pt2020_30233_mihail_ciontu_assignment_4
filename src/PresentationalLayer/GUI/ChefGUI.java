package PresentationalLayer.GUI;

import BusinessLayer.Restaurant;

import java.util.Observable;
import java.util.Observer;
/**
 * Implements observer, notified when waiter creates new order.
 */
public class ChefGUI implements Observer {
    private int orderId;
    Observable observable;
    public ChefGUI(Observable observable){
        this.observable=observable;
        observable.addObserver(this);
    }

    @Override
    public void update(Observable o, Object arg) {
        if(o instanceof Restaurant)
        {
            Restaurant restaurant=(Restaurant) o;
            this.orderId=restaurant.getOrderId();
            display();
        }
    }
    public void display(){
        System.out.println("New order "+orderId);
    }
}
