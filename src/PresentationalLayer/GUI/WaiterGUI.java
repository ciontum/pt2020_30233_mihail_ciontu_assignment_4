package PresentationalLayer.GUI;
import BusinessLayer.Product.MenuItem;
import BusinessLayer.Order;
import PresentationalLayer.GUI.GeneralUI;
import PresentationalLayer.Initializer;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import java.util.ArrayList;
import java.util.Date;
/**
 * Waiter front-end ,creates orders and displays them on UI
 */
public class WaiterGUI extends GeneralUI {
    public AnchorPane WaiterPanel;
    private VBox ordersBox;
    private HBox hBox;
    private HBox utilityBox;
    private ObservableList<TablePosition> selectedCells;
    private TextField tableNumberField;
    private Button createOrderButton;
    private TableView table;
    private Initializer initializer;
    public WaiterGUI(){
        super();
        initializer=new Initializer(this);
    }

    public void showWaiterTable(ActionEvent actionEvent) {
        if(menuItemsObservableInstance==null)
        deserializeMenuItems();
        table=initializer.initializeTableWithUtilities();

        initializeTableAndOrdersHbox();

        VBox finalBox = initializeConstructedVbox();
        WaiterPanel.getChildren().add(finalBox);
    }
    public void initializeUtilityBox(){
        try {
            utilityBox=addUtilBox(new Object[]{tableNumberField,createOrderButton});
        }
        catch (Exception err){
            System.out.println("Invalid componet");
        }
    }
    public void initializeOrdersVbox(){
        ordersBox=new VBox();
        ordersBox.getChildren().add(new Label("Orders"));
        ordersBox.setSpacing(5);
        ordersBox.setPadding(new Insets(10,10,10,10));
    }
    public void initializeTableAndOrdersHbox(){
        hBox=new HBox();
        hBox.getChildren().addAll(table,ordersBox);
    }
    private VBox initializeConstructedVbox(){
        VBox vbox = new VBox();
        vbox.setPadding(new Insets(50, 0, 0, 10));
        vbox.getChildren().addAll(hBox,utilityBox);
        return vbox;
    }
    @Override
    public void initializeVerticalUtility(){
        tableNumberField=(TextField)nodeFactory("Table","TextField");
        tableNumberField.setMaxWidth(50);

        createOrderButton=(Button)nodeFactory("Create Order","Button");

    }
    @Override
    public void initializeMultiSelectMode(TableView table){
        table.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        table.getSelectionModel().setCellSelectionEnabled(true);

        selectedCells=onMultiSelect(table);
    }
    public void initializeActions(){
        createOrderAction();
    }
    private void createOrderAction(){
        createOrderButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                if(selectedCells.size()==0)
                    return;
                ArrayList<MenuItem> orderItems=constructOrderedItems();
                if(createOrder(Integer.parseInt(tableNumberField.getText()),orderItems))
                constructBoxForDisplay(orderItems);
                else
                    return;
            }
        });
    }
    private ArrayList<MenuItem> constructOrderedItems(){
        ArrayList<MenuItem> orderItems=new ArrayList<>();
        for(int i=0;i<selectedCells.size();i++) {
            MenuItem orderItem=((MenuItem)table.getSelectionModel().getSelectedItems().get(i));
            orderItems.add(orderItem);
        }
        selectedCells.clear();
        return orderItems;
    }
    private void constructBoxForDisplay(ArrayList<MenuItem> orderItems){
        VBox orderBox=constructOrderVBox(orderItems,Integer.parseInt(tableNumberField.getText()));
        displayOrder(orderBox);
    }
    private void displayOrder(VBox vBox){
        ordersBox.getChildren().add(vBox);
    }

    private VBox constructOrderVBox(ArrayList<MenuItem> orderedItems,int tableNumber){
        VBox orderBox=new VBox();
        int computedPrice=0;
        Text tableNumberText=new Text("Table "+tableNumber);
        tableNumberText.setUnderline(true);
        orderBox.getChildren().add(tableNumberText);

        for(int i=0;i<orderedItems.size();i++){
            computedPrice += orderedItems.get(i).computePrice();
            orderBox.getChildren().add(getOrderedItemText(orderedItems.get(i)));
        };

        orderBox.getChildren().add(getComputedOrderPriceText(computedPrice));

        orderBox.getChildren().add(constructBillGeneratorButton(tableNumber,orderBox));

        orderBox.setSpacing(10);

        return orderBox;
    }
    private Button constructBillGeneratorButton(int tableNumber,VBox orderBox){
        final Button generateBill=(Button)nodeFactory("Generate Bill","Button");
        generateBill.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                commands.generateBill(tableNumber);
                ordersBox.getChildren().remove(orderBox);
            }
        });
        return generateBill;
    }
    private Text getOrderedItemText(MenuItem orderedItem){
        Text orderedItemText=new Text(orderedItem.getName()+" price="+orderedItem.getPrice());
        return orderedItemText;
    }
    private Text getComputedOrderPriceText(int computedPrice ){
        return new Text ("Total="+computedPrice);
    }
    public boolean createOrder(int tableNumber,ArrayList<MenuItem> selectedItems){
        Order newOrder=new Order(new Date(),tableNumber);
        boolean created=commands.createOrder(selectedItems,newOrder);
        return created;
    }
}
