package PresentationalLayer.GUI;

import BusinessLayer.Product.MenuItem;
import BusinessLayer.Product.CompositeProduct;
import PresentationalLayer.Initializer;
import com.google.java.contract.InvariantError;
import com.google.java.contract.Requires;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import javax.naming.directory.InvalidAttributeIdentifierException;

/**
 * Admin front-end , adds ,deletes and edits menu's UI
 */
public class AdminGUI extends GeneralUI {
    private TableView table;
    public  AnchorPane AdminPanel;
    private HBox addBaseProductBox;
    private HBox addComposedProductBox;
    private HBox utilityBox;
    public  ToolBar AdminToolBar;
    private TextField newProductNameField;
    private TextField newPriceField;
    private Button newProductButton;
    private TextField newCompositeField;
    private Button newCompositeButton;
    private Button multiSelect;
    private Button deleteProductButton;
    private ObservableList<TablePosition> selectedCells;
    private Initializer initializer;
    public AdminGUI() {
        super();
        initializer=new Initializer(this);
    }

    private  void deleteMenuItemList(String name){

        for(int i=0;i<menuItemsObservableInstance.getMenuItemsList().size();i++)
            if(menuItemsObservableInstance.getMenuItemsList().get(i).getName()==name)
                menuItemsObservableInstance.removeMenuItemFromList(menuItemsObservableInstance.getMenuItemsList().get(i));
    }
    @Requires("name!=null,price>0")
    private  void addMenuItemsList(String name,int price) {
            menuItemsObservableInstance.addMenuItemToList(productFactory.createProduct("base",name,price));
    }

    public void showAdminTable(ActionEvent actionEvent) {
        deserializeMenuItems();

        table=initializer.initializeTableWithUtilities();

        initializeUtilities();

        VBox finalBox = initializeConstructedVbox();

        AdminPanel.getChildren().add(finalBox);
    }
    public void initializeUtilities(){

        try {
            addBaseProductBox=addUtilBox(new Object[]{newProductNameField,newPriceField,newProductButton});
            addComposedProductBox=addUtilBox(new Object[]{newCompositeField,newCompositeButton});
            utilityBox=addUtilBox(new Object[]{multiSelect,deleteProductButton});
        }
        catch (Exception err){
            System.out.println("Invalid componet");
        }
    }
    private VBox initializeConstructedVbox(){
        VBox vbox=new VBox();
        vbox.setPadding(new Insets(50, 0, 0, 10));
        vbox.getChildren().addAll(table, addBaseProductBox,addComposedProductBox,utilityBox);
        return vbox;
    }
    @Override
    public void initializeVerticalUtility(){
        newProductNameField=(TextField)nodeFactory("Product Name","TextField");
        newProductNameField.setMaxWidth(300);

        newPriceField=(TextField)nodeFactory("Price","TextField");
        newPriceField.setMaxWidth(50);

        newProductButton=(Button) nodeFactory("Add Base","Button");

        newCompositeField=(TextField)nodeFactory("Product Name","TextField");
        newCompositeField.prefWidth(300);

        newCompositeButton=(Button)nodeFactory("Add Composite","Button");

        multiSelect=(Button)nodeFactory("Single-select","Button");

        deleteProductButton=(Button)nodeFactory("Delete Product","Button");

    }
    @Override
    public void initializeMultiSelectMode(TableView table){
        table.getSelectionModel().setCellSelectionEnabled(true);

        selectedCells=onMultiSelect(table);

        multiSelect.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                multiSelect.setText((multiSelect.getText().equals("Multi-select"))?"Single-select":"Multi-select");
                if(multiSelect.getText().equals("Multi-select"))
                    table.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
                else
                    table.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
                selectedCells.clear();
            }
        });
    }
    public void initializeActions(){
        addComposedProductAction();
        addBaseProductAction();
        deleteProductAction();
    }
    private void addComposedProductAction(){
        newCompositeButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                if(selectedCells.size()==0)
                    return;
                MenuItem compositeMenuItem=constructComposedMenuItem();

                commands.addMenuItem(compositeMenuItem.getName(),compositeMenuItem.computePrice(),true);

                addMenuItemsList(compositeMenuItem.getName(),compositeMenuItem.computePrice());
            }
        });
    }
    private void addBaseProductAction(){
        newProductButton.addEventHandler(MouseEvent.MOUSE_CLICKED, (event) -> {
            String insertedName = (newProductNameField).getText();
            int insertedPrice = Integer.parseInt(newPriceField.getText());
            try {
                commands.addMenuItem(insertedName, insertedPrice, true);
            }catch(InvariantError error){
                System.out.println(error);
            }
            addMenuItemsList(insertedName,insertedPrice);
            newProductNameField.clear();
            newPriceField.clear();
        });
    }
    private void deleteProductAction(){
        deleteProductButton.addEventHandler(MouseEvent.MOUSE_CLICKED, (event) -> {
            String nameToDelete=((MenuItem)table.getSelectionModel().getSelectedItems().get(0)).getName();
            commands.deleteMenuItem(nameToDelete);
            deleteMenuItemList(nameToDelete);
        });
    }

    private MenuItem constructComposedMenuItem(){
        MenuItem compositeMenuItem=productFactory.createProduct("composed",newCompositeField.getText(),0);
        for(int i=0;i<selectedCells.size();i++) {
            MenuItem productToCompose=((MenuItem)table.getSelectionModel().getSelectedItems().get(i));
            ((CompositeProduct)compositeMenuItem).add(productToCompose);
        }
        return compositeMenuItem;
    }

}
